import React, {Component} from 'react';
import Reveal from 'react-reveal/Reveal';


let isDesktop;

class AgencyBanner extends Component {
    constructor(props) {
        super(props);
        this.state = {
          isDesktop: false,
        };
    }
    componentDidMount() {
        if (window.innerWidth > 769) {
          this.setState({ isDesktop: true });
          isDesktop = true;
        } else {
          this.setState({ isDesktop: false });
          isDesktop = false;
        }
      }

    render(){
        let BannerData = this.props.BannerData;
        return(
            <section className="agency_banner_area bg_color">
                <img className="banner_shap" style={{width:'100%'}} src={this.state.isDesktop ? "https://res.cloudinary.com/dyqcevdpm/image/upload/v1575016026/SaasLand/desktop_homepage_hero_image3_extended.jpg" : "https://res.cloudinary.com/dyqcevdpm/image/upload/v1575441240/SaasLand/desktop_homepage_hero_image3_for_small_devices.png"} alt="" />
                <div className="container custom_container">
                    <div className="row">
                        <div className="col-lg-5 d-flex justify-content-center">
                            <div className="agency_content">
                            <Reveal effect="fadeInUp">
                                {
                                    BannerData.AgencyText.map(Agency =>{
                                        return(
                                            <React.Fragment key={Agency.id}>
                                                <h2 className="f_700 t_color3 mb_40 wow fadeInLeft" data-wow-delay="0.3s">{Agency.btitle}</h2>
                                                <p className="f_4001 l_height28 wow fadeInLeft" data-wow-delay="0.4s">{Agency.description}</p>
                                            </React.Fragment>
                                        )
                                    })
                                }
                                <div className="action_btn d-flex align-items-center mt_60">
                                        <a href="/Pages/submitform" className="btn_hover agency_banner_btn wow fadeInLeft" data-wow-delay="0.5s">START HAIR QUIZ</a> 
                                </div>
                                </Reveal>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
export default AgencyBanner;