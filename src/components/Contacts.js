import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';

class Contacts extends Component {
    constructor(props){
        super(props)
        this.state={
            name:"",email:"",contactNo:"",address1:"",address2:"",pincode:"",landmark:""
        }
    }
nameHandler=(e)=>{
this.setState({name:e.target.value})
}
emailHandler=(e)=>{
    this.setState({email:e.target.value})
}
contactHandler=(e)=>{
    this.setState({contactNo:e.target.value})
}
address1Handler=(e)=>{
    this.setState({address1:e.target.value})
}
address2Handler=(e)=>{
    this.setState({address2:e.target.value})
}

pinHandler=(e)=>{
    this.setState({pincode:e.target.value})
}

landHandler=(e)=>{
    this.setState({landmark:e.target.value})
}

submitHandler=(e)=>{
  e.preventDefault()
    // alert("anubhav")
    // console.log("11111",this.state.name,this.state.email,this.state.pincode)
this.props.history.push("/Pages/ConfirmOrder")

}
    render(){
       console.log(this.props,"666666")
        return(
            <section className="contact_info_area sec_pad bg_color">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3">
                            <div className="contact_info_item">
                                <h6 className="f_p f_size_20 t_color3 f_500 mb_20">Office Address</h6>
                                <p className="f_400 f_size_15">Melbourne’s GPO 434 Bourke St. Dhaka VIC 3074, Australia</p>
                            </div>
                            <div className="contact_info_item">
                                <h6 className="f_p f_size_20 t_color3 f_500 mb_20">Contact Info</h6>
                                <p className="f_400 f_size_15"><span className="f_400 t_color3">Phone:</span> <a href="tel:3024437488">(+096) 302 443 7488</a></p>
                                <p className="f_400 f_size_15"><span className="f_400 t_color3">Fax:</span> <a href="tel:3024437488">(+096) 204 353 6684</a></p>
                                <p className="f_400 f_size_15"><span className="f_400 t_color3">Email:</span> <a href="mailto:saasland@gmail.com">saasland@gmail.com</a></p>
                            </div>
                        </div>
                        <div className="contact_form col-lg-9">
                            <h2 className="f_p f_size_22 t_color3 f_600 l_height28 mb_40">Leave a Message</h2>
                            <form  className="contact_form_box" onSubmit ={this.submitHandler} >
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="form-group text_box">
                                            <input required type="text" id="name" name="name" placeholder="Your Name" onChange={(e)=>{this.props.changeName(e.target.value)}}/>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group text_box">
                                            <input required type="email" name="email" id="email" placeholder="Your Email"onChange={(e)=>{this.props.changeEmail(e.target.value)}}/>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group text_box">
                                            <input type="text" id="contact" name="contact" placeholder="Contact*" onChange={(e)=>{this.props.changeNo(e.target.value)}}/>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group text_box">
                                            <input type="text" id="address1" name="address1" placeholder="Address Line 1*" onChange={(e)=>{this.props.changeAddress1(e.target.value)}}/>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group text_box">
                                            <input type="text" id="address2" name="address2" placeholder="Address Line 2*" onChange={(e)=>{this.props.changeAddress2(e.target.value)}}/>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group text_box">
                                            <input type="text" id="landmark" name="landmark" placeholder="Landmark" onChange={(e)=>{this.props.changeLandmark(e.target.value)}}/>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group text_box">
                                            <input type="text" id="pincode" name="pincode" placeholder="Pincode" onChange={(e)=>{this.props.changePincode(e.target.value)}}/>
                                        </div>
                                    </div>
                                </div>
                                
                                <button type="submit" className="btn_three"  >Submit</button>
                            </form>
                            <div id="success">Your message succesfully sent!</div>
                            <div id="error">Opps! There is something wrong. Please try again</div>
                        </div>
                    </div>
                    
                </div>
            </section>
        )
    }
}
const mapStateToProps=(state)=>{
return state
}
const mapDispatchToProps=(dispatch)=>{
    
    return{
        changeName:(name)=>{dispatch({type:'CHANGE_NAME',payload:name})},
        changeEmail:(email)=>{dispatch({type:'CHANGE_EMAIL',payload:email})},
        changeNo:(number)=>{dispatch({type:'CHANGE_NO',payload:number})},
        changeAddress1:(address1)=>{dispatch({type:'CHANGE_ADDRESS1',payload:address1})},
        changeAddress2:(address2)=>{dispatch({type:'CHANGE_ADDRESS2',payload:address2})},
        changePincode:(pin)=>{dispatch({type:'CHANGE_PIN',payload:pin})},
        changeLandmark:(land)=>{dispatch({type:'CHANGE_LAND',payload:land})},
    }
    }
export default connect(mapStateToProps,mapDispatchToProps)(withRouter(Contacts));