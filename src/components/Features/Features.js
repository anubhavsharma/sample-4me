import React, {Component} from 'react';
import Featuresitems from './Featuresitems';

class Features extends Component{
    render(){
        var {aClass} =this.props;
        return(
            <section className={`agency_featured_area bg_color ${aClass}`}>
                <div className="container">
                    <h2 className="f_size_30 f_600 t_color3 l_height40 text-center wow fadeInUp" data-wow-delay="0.3s">How It Works</h2>
                    <div className="features_info">
                        <img className="dot_img" src={require ('../../img/home4/dot.png')} alt=""/>
                        <Featuresitems rowClass="row flex-row-reverse" aClass="pr_70 pl_70" fimage="img_step_one.png" iImg="icon1.png" ftitle="TAKE OUR FREE SKIN GENOME QUIZ" descriptions="Your answers allow us to analyze your skin, lifestyle, and environment"/>
                        <Featuresitems rowClass="row agency_featured_item_two" aClass="pl_100" fimage="img_step_two.png" iImg="icon2.png" ftitle="Discover Your Formulations" descriptions="We identify the most effective, scientifically proven ingredients for your skin using our award-winning technology"/>
                        <Featuresitems rowClass="row flex-row-reverse" aClass="pr_70 pl_70" fimage="img_step_three.png" iImg="icon3.png" ftitle="Get Your Skincare" 
                        descriptions="Using these unique ingredients, we create 3 custom skincare products exclusively for you"/>
                        <div className="dot middle_dot"><span className="dot1"></span><span className="dot2"></span></div>
                    </div>
                </div>
            </section>
        )
    }
}
export default Features;