import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { Typography, Container } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';


// const styles = () => ({
// 	root: {
// 		flexGrow: 1,
//   },
//   card: {
//     maxWidth: 745,
//   },
//   media: {
//     height: 0,
//     paddingTop: '56.25%', // 16:9,
//     marginTop:'30'
//   }
// });

const styles = 
{

media: {
  height: 0,
  paddingTop: '56.25%', // 16:9,
  marginTop:'30'
}
  };

class SliderPro extends Component {
  render() {
    const classes = styles;
    return (
          <Container maxWidth="lg">
            <Grid container direction="row" justify="center" alignItems="center">
              <Grid item lg={4} md={4} sm={12} xs={12}>
                <div>
                  <Typography variant="h4">
                    CLEAN, SCIENCE- BACKED INGREDIENTS
              </Typography>
                  <Typography variant="body1">
                    Every product we formulate is free of sulfates, parabens, phthalates, mineral oils, gluten, and is always vegan + cruelty-free.
              </Typography>
                </div>
              </Grid>
              <Grid item lg={2} md={2} sm={6} xs={6}>
                <div>
                  <img src="https://res.cloudinary.com/dyqcevdpm/image/upload/v1577691630/SaasLand/ic_cruelty_free.png" alt="cruelty free" />
                </div>
                <div>
                  <Typography variant="body2">
                    Cruelty-Free
              </Typography>
                </div>
              </Grid>
              <Grid item lg={2} md={2} sm={6} xs={6}>
                <div>
                  <img src="https://res.cloudinary.com/dyqcevdpm/image/upload/v1577691635/SaasLand/ic_vegan.png" alt="vegan" />
                </div>
                <div>
                  <Typography variant="body2">
                    100% Vegan
              </Typography>
                </div>
              </Grid>
              <Grid item lg={2} md={2} sm={6} xs={6}>
                <div>
                  <img src="https://res.cloudinary.com/dyqcevdpm/image/upload/v1577689857/SaasLand/ic_paraben_free.png" alt="Paraben" />
                </div>
                <div>
                  <Typography variant="body2">
                    Paraben + Sulphate free
              </Typography>
                </div>
              </Grid>
              <Grid item lg={2} md={2} sm={6} xs={6}>
              <div>
                <img src="https://res.cloudinary.com/dyqcevdpm/image/upload/v1577691708/SaasLand/ic_made_in_india.png" alt="Made in India" />
              </div>
              <div>
                <Typography variant="body2">
                  Made in India
            </Typography>
              </div>
            </Grid>
            </Grid>

          <Grid container direction="row" justify="center" alignItems="center">
            <Grid item>
            <Typography variant="h3"> 4Me Pro Tips</Typography>
            </Grid>
          </Grid>
          <Grid container direction="row" justify="space-evenly" alignItems="center" spacing={2}>
            <Grid item xl={4} lg={4} md={4} sm={12} xs={12}>
              <Card className={classes.card}>
              <CardActionArea>
                <CardMedia
                className={classes.media}
                  image='https://res.cloudinary.com/dyqcevdpm/image/upload/v1577701253/SaasLand/pjimage-9-1556525020.png'
                  title="Contemplative Reptile"
                  style={styles.media}
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diamundefined
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                  Every product we formulate is free of sulfates, parabens, phthalates, mineral oils, gluten, and is always vegan + cruelty-free…
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="primary">
                  Learn More
                </Button>
              </CardActions>
            </Card>
            </Grid>

            <Grid item xl={4} lg={4} md={4} sm={12} xs={12}>
              <Card className="">
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image="https://res.cloudinary.com/dyqcevdpm/image/upload/v1577701255/SaasLand/84mimjh_hair-care-_625x300_14_September_19.png"
                  title="Contemplative Reptile"
                  style={styles.media}
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diamundefined
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                  Every product we formulate is free of sulfates, parabens, phthalates, mineral oils, gluten, and is always vegan + cruelty-free…
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="primary">
                  Learn More
                </Button>
              </CardActions>
            </Card>
            </Grid>

            <Grid item xl={4} lg={4} md={4} sm={12} xs={12}>
              <Card className="">
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image="https://res.cloudinary.com/dyqcevdpm/image/upload/v1577701251/SaasLand/hair-care_5200105_835x547-m.png"
                  title="Contemplative Reptile"
                  style={styles.media}
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diamundefined
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                  Every product we formulate is free of sulfates, parabens, phthalates, mineral oils, gluten, and is always vegan + cruelty-free…
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="primary">
                  Learn More
                </Button>
              </CardActions>
            </Card>
            </Grid>
          </Grid>
          
          </Container>


    );
  }
}

export default SliderPro;