import React, { Component } from 'react';
import CustomNavbar from '../components/CustomNavbar';
import FooterData from '../components/Footer/FooterData';
import FooterTwo from '../components/Footer/FooterTwo';
import { ReactTypeformEmbed } from 'react-typeform-embed';

class submitform extends Component {
    render() {
        return (
                <div>
                    <CustomNavbar slogo="sticky_logo" mClass="menu_four" nClass="w_menu ml-auto mr-auto" />
                    <ReactTypeformEmbed url="https://4me.typeform.com/to/A3qtmE" />
                    
            </div>
        )
    }
}

export default submitform;